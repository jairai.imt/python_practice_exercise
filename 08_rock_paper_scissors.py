#!/usr/bin/python

import random

def rock_paper_scissors(input1, input2):
    rps_dict = {1: 'Rock', 2: 'Paper', 3: 'Scissor'}
    
    print("Player 1: " + str(rps_dict[input1]))
    print("Player 2: " + str(rps_dict[input2]))
    
    if(input1 == input2):
        return 0
    if(rps_dict[input1] == "Rock" and rps_dict[input2] == "Scissor"):
        return 1
    elif(rps_dict[input1] == "Paper" and rps_dict[input2] == "Rock"):
        return 1
    elif(rps_dict[input1] == "Scissor" and rps_dict[input2] == "Paper"):
        return 1
    elif(rps_dict[input2] == "Rock" and rps_dict[input1] == "Scissor"):
        return 2
    elif(rps_dict[input2] == "Paper" and rps_dict[input1] == "Rock"):
        return 2
    elif(rps_dict[input2] == "Scissor" and rps_dict[input1] == "Paper"):
        return 2

def user_play(player):
    print("Player " + str(player) + ":")
    print("1. Rock")
    print("2. Paper")
    print("3. Scissors")
        
    return input()
    
def comp_play(): return random.randint(1,3)

    
def one_player_rps():
    count = 0
    for x in range(1,4):
        print("")
        print("--------------------------------------------------------")
        print("Game " + str(x) + ":\n\n")
        
        val1 = comp_play()
        val2 = user_play(1)
        
        result = rock_paper_scissors(val1,val2)
        print("----------------------Result--------------------------")
        if result == 1:
            print("Game " + str(x) + " won by Computer")
            count = count + 1
        elif result == 2:
            print("Game " + str(x) + " won by Player")
            count = count - 1
        else:
            print("Game " + str(x) + " is Draw")
            continue
    if count == 0: print(" Draw")
    if count > 0: print(" Computer wins")
    if count < 0: print(" Player wins")
        
def two_player_rps():
    count = 0
    for x in range(1,4):
        print("")
        print("--------------------------------------------------------")
        print("Game " + str(x) + ":\n\n")
        
        val1 = user_play(1)
        val2 = user_play(2)
        
        result = rock_paper_scissors(val1,val2)
        
        print("----------------------Result--------------------------")
        if result == 1:
            print("Game " + str(x) + " won by Player 1")
            count = count + 1
        elif result == 2:
            print("Game " + str(x) + " won by Player 2")
            count = count - 1
        else:
            print("Game " + str(x) + " is Draw")
            continue
    if count == 0: print(" Draw")
    if count > 0: print(" Player 1 wins")
    if count < 0: print(" Player 2 wins")
    
def rps_menu():
    while(True):
        print("--------------------------------------------------------")
        print("1. Play against Computer")
        print("2. Play against Player 2")
        print("3. Exit")
        
        choice = input()
    
        if(choice == 1):
            one_player_rps()
        elif(choice == 2):
            two_player_rps()
        elif(choice == 3): break
    
def menu():
    print("--------------------------------------------------------")
    print("\n\nLet's start playing 'Rock Paper Scissors' woooo!!")
    print("1. Play")
    print("2. Exit")
    print("--------------------------------------------------------")
    
    choice = input()
    
    if(choice == 1):
        rps_menu()
    
if __name__ == "__main__":
    menu()