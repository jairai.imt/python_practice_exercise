#!/usr/bin/python

a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

b = [x for x in a if x%2 == 0]
print("List consisiting of only even numbers is: " +  str(b))