#!/usr/bin/python

import random

menu_dict ={1: 'Guess a number between 1 and 9',
            2: 'Exit',
            3: 'Invalid Choice',
            4: 'You guessed exactly right',
            5: 'You guessed too low',
            6: 'You guessed too high'}

def menu():
    guess_count = 0
    right = 0
    while(True):
        print(menu_dict[1])
        print(menu_dict[2])
    
        choice = raw_input()
    
        if(choice.lower() == 'exit'):
            print("No of Guesses: " + str(guess_count))
            print("Correct Guesses: " + str(right))
            break    
        
        elif(1<=int(choice)<=9 ): 
            guess_count +=1
            rand = random.randint(1,9) 
            
            if(int(choice) == rand): 
                print(menu_dict[4]) 
                right +=1
            elif(int(choice) < rand): print(menu_dict[5])
            else: print(menu_dict[6])
                
        else: print(menu_dict[3])
    
if __name__ == "__main__":
    menu()