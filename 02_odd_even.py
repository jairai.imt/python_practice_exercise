#!/usr/bin/python

def get_quotient(dividend, divisor): return dividend / divisor
    
def get_remainder(dividend, divisor): return dividend % divisor

def isfactor(dividend, divisor):
    if (get_remainder(dividend, divisor) == 0): return True
    else: return False
    


def odd_even():  
    number = input("Provide a number: ")
    
    
    if (isfactor(number, 4)):
        return str(number) + " is divisible by 4"
    elif (isfactor(number, 2)):
        return str(number) + " is a even number, but not divisible by 4"
    else:
        return str(number) + " is a odd number"
        

def modulo_check():
    dividend = input("Provide the dividend: ")
    divisor  = input("Provide the divisor: ")
  
    if (isfactor(dividend, divisor)): msg = ""    
    else: msg = " not"
        
    return str(divisor) + " is" + msg + " a factor of " + str(dividend)

def menu():
    while(True):
        print("\n\n")
        print("1. Do you want to figure out if a number is even/odd, or divisible by four?")
        print("2. Do you want to know remainder of a division?")
        print("3. Continue")
        print("Press any key to exit!!")
    
        choice = input()
        msg = ""
    
        if choice == 1: msg = odd_even()
        
        elif choice == 2: msg = modulo_check()
        
        elif choice == 3: continue
        
        else: break
    
        print(msg)
  
        
if __name__ == "__main__":
    menu()
